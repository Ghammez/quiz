package com.example.mohamed.tp1_quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by mohamed on 12/10/2018.
 */

public class LoginActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        final EditText mail = (EditText) findViewById(R.id.mail);
        final EditText password = (EditText) findViewById(R.id.password);
        Button connection = (Button) findViewById(R.id.connection);

        connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(mail.getText().toString().equals("med") && password.getText().toString().equals("med"))
               {
                   Intent toQuiz = new Intent(getApplicationContext(),MainActivity.class);
                   startActivity(toQuiz);
               }
            }
        });
    }
}
