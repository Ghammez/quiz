package com.example.mohamed.tp1_quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView tv_reponse = (TextView) findViewById(R.id.R1);
        final RadioButton vrai = (RadioButton) findViewById(R.id.vrai);
        final RadioButton faux = (RadioButton) findViewById(R.id.faux);
        final CheckBox checkBox_Politique = (CheckBox) findViewById(R.id.cb1);
        final CheckBox checkBox_Ecologique = (CheckBox) findViewById(R.id.cb2);
        final CheckBox checkBox_Économique = (CheckBox) findViewById(R.id.cb3);
        Button ok = (Button) findViewById(R.id.ok);
        Button reset = (Button) findViewById(R.id.btn_reset);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int s=0;
               if( tv_reponse.getText().toString().equals("1987") )
               {
                    s++;
               }
                if(vrai.isChecked())
                {
                    s++;
                }
                if(checkBox_Ecologique.isChecked() && checkBox_Économique.isChecked() && !checkBox_Politique.isChecked())
                {
                    s++;
                }
                Toast.makeText(getApplicationContext()," Votre score est "+s,Toast.LENGTH_SHORT).show();
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_reponse.setText("");
                vrai.setChecked(false);
                faux.setChecked(false);
                checkBox_Ecologique.setChecked(false);
                checkBox_Politique.setChecked(false);
                checkBox_Économique.setChecked(false);
            }
        });
    }



}
