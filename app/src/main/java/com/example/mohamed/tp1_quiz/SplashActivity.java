package com.example.mohamed.tp1_quiz;

import android.content.Intent;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
/**
 * Created by mohamed on 12/10/2018.
 */

public class SplashActivity extends Activity {
    private static int SPLASH_TIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent tologin = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(tologin);
                finish();
            }
        },SPLASH_TIME);
    }
}
