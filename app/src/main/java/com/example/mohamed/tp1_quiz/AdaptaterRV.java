package com.example.mohamed.tp1_quiz;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by mohamed on 12/10/2018.
 */

public class AdaptaterRV extends RecyclerView.Adapter {
    private Context context;
    private String[] names;

    public AdaptaterRV(Context context, String[] names) {
        this.context = context;
        this.names = names;
    }

    public AdaptaterRV(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
