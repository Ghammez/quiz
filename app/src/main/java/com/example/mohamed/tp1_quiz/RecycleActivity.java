package com.example.mohamed.tp1_quiz;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by mohamed on 12/10/2018.
 */

public class RecycleActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerview);

        RecyclerView recycler_view = (RecyclerView) findViewById(R.id.recycler_view);

        String[] names = new String[5];
        names[0]= "mohamed";
        names[1]= "hamza";
        names[2]= "khairi";
        names[3]= "yassine";
        names[4]= "akrem";

        recycler_view.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        AdaptaterRV adaptater = new AdaptaterRV(getApplicationContext(),names);
        recycler_view.setAdapter(adaptater);
    }
}
